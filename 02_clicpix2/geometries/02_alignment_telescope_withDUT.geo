[Timepix3_0]
material_budget = 0.00075
number_of_pixels = 256, 256
orientation = 11.0694deg,186.381deg,-1.20573deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 907.651um,287.285um,0
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_1]
material_budget = 0.00075
number_of_pixels = 256, 256
orientation = 11.2904deg,186.686deg,-0.957412deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -280.859um,400.974um,21.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_2]
material_budget = 0.00075
number_of_pixels = 256, 256
orientation = 10.5845deg,187.075deg,-1.54544deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 5.292um,378.368um,43.5mm
role = "reference"
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[CLICpix2_0]
mask_file = "mask_clicpix2.conf"
number_of_pixels = 128, 128
orientation = -0.132181deg,0.0513943deg,-1.29683deg
orientation_mode = "xyz"
pixel_pitch = 25um,25um
position = -691.852um,304.236um,106mm
role = "dut"
spatial_resolution = 4um,4um
time_resolution = 1ms
type = "clicpix2"

[Timepix3_3]
material_budget = 0.00075
number_of_pixels = 256, 256
orientation = 9.06654deg,9.09049deg,0.00429718deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -24.216um,-9.772um,186.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_4]
material_budget = 0.00075
number_of_pixels = 256, 256
orientation = 7.90624deg,9.80921deg,1.27976deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 412.842um,-587.951um,208.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

[Timepix3_5]
material_budget = 0.00075
number_of_pixels = 256, 256
orientation = 8.06352deg,10.113deg,0.100841deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -1.10118mm,-19.652um,231.5mm
spatial_resolution = 4um,4um
time_resolution = 1.5ns
type = "timepix3"

