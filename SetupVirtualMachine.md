# Setting up the Virtual Machine

If you want to give **Allpix Squared** or **Corryvreckan** a quick try without installing it on your system, you can use the Virtual Machine image that has been prepared for the BTTB9 tutorials.
Thanks to [VirtualBox](https://www.virtualbox.org/), this solution works for Linux, Windows and Mac.
The image contains a compressed version of Ubuntu including ROOT, Allpix Squared, Corryvreckan, and all dependencies.
Note that your PC should have **free disk space of ≥20 GB** and **at least 4 GB of RAM**.
To set it up, follow the instructions below:

* Download and install **VirtualBox 6.1** on your PC: https://www.virtualbox.org/
* Download the virtual machine image: https://cern.ch/corryvreckan/VMs/AllpixSquared_Corry_Ubuntu_Minimal.ova  
Note that this may take a while because of its size of ~12 GB.
* Open VirtualBox and click *File -> Import Appliance*.  
Choose the downloaded `*.ova` file and accept all defaults.
* Once the import is complete, start the virtual machine by double-clicking it. You should be logged in automatically.  
You can now open a terminal and run `allpix` or `corry`. If you want to install further packages: `user = "student"`, `password = "allpixsquared"`

## Troubleshooting:
* **For all Mac users:** If you get the error message *"Kernel driver not installed (rc=-1908)*, please follow the instructions provided here:   
https://www.howtogeek.com/658047/how-to-fix-virtualboxs-"kernel-driver-not-installed-rc-1908-error/
* If you cannot boot the virtual machine and get an error like *"VT-x is disabled in the BIOS for all CPU modes"* you may need to adapt your BIOS settings: Reboot your PC, enter the BIOS options and enable *"Intel Virtualization Technology"* or *"AMD Secure Virtual Machine (SVM)"* under *Advanced CPU Settings* or similar.
* If you encounter an error message like *Result Code:NS_ERROR_INVALID_ARG (0x80070057)*, this may indicate that you don't have enough disk space on your PC.
* **For Windows 10 users:** If your VM crashes at the booting stage, try the following: Open VirtualBox and click *File -> Virtual Media Manager...* and convert the `*.vdi` file (which was obtained after importing the `*.ova`) to `*.vmdk` (which is a VMware player/VMware workstation disk file). Then create a new virtual machine as usual but as a hard disk use the new `*.vmdk` file.

## Some tips:
* Once you are logged in, you may adjust the screen resolution by right-clicking on the desktop and then *Display Settings -> Resolution*.  
* If your personal PC allows you may increase the RAM for the virtual machine by clicking *Settings -> System -> Motherboard* and changing the Base Memory before booting the virtual machine.
